package restaurante;

public class Plato extends Menu{
    private String descripcionDelPlato;

    public Plato(double precio, String categoria, int cantidadPedida, String pedido, String descripcion){
        super(precio, categoria, pedido,cantidadPedida);
        this.descripcionDelPlato=descripcion;
    }

    // --------------getter------------------
    public String getDescripcion() {
        return descripcionDelPlato;
    }

    //--------------setter------------------
    public void setDescripcion(String descripcion) {
        this.descripcionDelPlato=descripcion;
    }

    @Override
    public String toString(){
        return "Categoria: " + getCategoria() + ", Plato: " + getNombrePedido()
        + " Precio: " + getPrecio() + " Descripcion: " + getDescripcion();
    }
}
