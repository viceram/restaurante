package restaurante;

public abstract class Pago {
    private double totalPagar;

    public double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(double totalPagar) {
        this.totalPagar = totalPagar;
        this.efectuarPago();
    }

    public abstract String efectuarPago();
}
