package restaurante;

public class Menu {
    private double precio;
    private String categoria;
    private int cantidadPedida;
    private String nombrePedido;

    public Menu(double precio, String categoria, String pedido, int cantidadPedida) {
        this.precio = precio;
        this.categoria = categoria;
        this.nombrePedido = pedido;
        this.cantidadPedida = cantidadPedida;
    }

    // ------------getter-------------
    public double getPrecio() {
        return precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public int getCantidadPedida() {
        return cantidadPedida;
    }

    public String getNombrePedido() {
        return nombrePedido;
    }

    // -------------setter------------
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public void setCantidadPedida(int cantidadPedida) {
        this.cantidadPedida = cantidadPedida;
    }

    public void setNombrePedido(String pedido) {
        this.nombrePedido = pedido;
    }

    @Override
    public String toString(){
        return "Categoria: " + getCategoria() + " " + getNombrePedido() + " Precio: " + getPrecio(); 
    }
}
