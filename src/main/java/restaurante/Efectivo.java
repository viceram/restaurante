package restaurante;

public class Efectivo extends Pago{
    private double cantidadQueSePagara;

    public double getCantidadQueSePagara() {
        return cantidadQueSePagara;
    }

    public void setEfectivoParaPago(double efectivoParaPago) {
        this.cantidadQueSePagara = efectivoParaPago;
    }

    @Override
    public String efectuarPago() {
        double vueltoTrasPago;
        vueltoTrasPago=getCantidadQueSePagara() - getTotalPagar();

        return "Fueron recibidos " + getCantidadQueSePagara() 
        + " y se devolvio " + vueltoTrasPago;
    }
}
