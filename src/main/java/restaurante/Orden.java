package restaurante;

import java.time.LocalTime;
import java.util.ArrayList;

public class Orden {
    private String estadoDeOrden;
    private int numeroMesa;
    private int numeroComensales;
    private LocalTime horaInicio;
    private LocalTime horaCierre;
    private ArrayList<Menu> ordenDelMenuSolicitada = new ArrayList<Menu>();
    private double totalAPagar;
    private Persona mozo;

    public Orden(int numeroMesa, Persona mozo, LocalTime horaInicio) {
        this.estadoDeOrden = "Abierta";
        this.numeroMesa = numeroMesa;
        this.mozo = mozo;
        this.horaInicio = horaInicio;
    }

    // --------------setter-----------------
    public void setNumeroMesa(int numeroMesa){
        this.numeroMesa=numeroMesa;
    }

    public void setEstadoOrden(String estadoDeOrden){
        this.estadoDeOrden=estadoDeOrden;
    }

    public void setNumComensales(int numeroComensales){
        this.numeroComensales=numeroComensales;
    }

    public void setHoraInicio(LocalTime horaInicio){
        this.horaInicio=horaInicio;
    }

    public void setOrdenSolicitada(Menu ordenSolicitada){
        this.ordenDelMenuSolicitada.add(ordenSolicitada);
    }

    public void setOrdenSolicitada(ArrayList<Menu> ordenSolicitada){
        this.ordenDelMenuSolicitada=ordenSolicitada;
    }

    public void setHoraCierre(LocalTime horaCierre){
        this.horaCierre=horaCierre;
    }

    public void setMozo(Persona mozo) {
        this.mozo = mozo;
    }

    //--------------getter-----------------
    public int getNumeroMesa(){
        return numeroMesa;
    }

    public String getEstadoOrden(){
        return estadoDeOrden;
    }

    public int getNumComensales(){
        return numeroComensales;
    }

    public LocalTime getHoraInicio(){
        return horaInicio;
    }

    public ArrayList<Menu> getOrdenSolicitada(){
        return ordenDelMenuSolicitada;
    }

    public LocalTime getHoraCierre(){
        return horaCierre;
    }

    public Persona getMozo() {
        return mozo;
    }

    public double getTotalAPagar(){
        return totalAPagar;
    }
    //---------------------------------------------
    public void calcularPrecioTotalDeOrden(){
        totalAPagar=0;
        for(int i=0; i<ordenDelMenuSolicitada.size(); i++){
            totalAPagar=totalAPagar + (ordenDelMenuSolicitada.get(i).getPrecio() *
            ordenDelMenuSolicitada.get(i).getCantidadPedida());
        }
    }

    public void cerrarOrden(String estadoFinal){
        LocalTime horaDeCierre= LocalTime.now();
        this.estadoDeOrden=estadoFinal;
        this.setHoraCierre(horaDeCierre);
    }

    public String datosDeLaOrdenAlAbrirla(){
        String nombreCompletoMozo = getMozo().getApellido() + getMozo().getNombre();

        String orden = "";

        for(int i=0; i<ordenDelMenuSolicitada.size(); i++){
            orden = orden + ordenDelMenuSolicitada.get(i).getCategoria() + " " + 
            ordenDelMenuSolicitada.get(i).getNombrePedido() + " "
            + ordenDelMenuSolicitada.get(i).getCantidadPedida();
        }

        return "Mesa: " + getNumeroMesa() + " Mozo:" + nombreCompletoMozo + " Nro Comensales: " +
            getNumComensales() + " Hora de incio:" + getHoraInicio().getHour() + " Lista de ordenes: "
            + orden + " Precio total" + getTotalAPagar();

    }

    public String datosDeLaOrdenAlCerrarla(){
        String nombreCompletoMozo = getMozo().getApellido() + getMozo().getNombre();

        String orden = "";

        for(int i=0; i<ordenDelMenuSolicitada.size(); i++){
            orden = orden + ordenDelMenuSolicitada.get(i).getCategoria() + " " + 
            ordenDelMenuSolicitada.get(i).getNombrePedido() + " "
            + ordenDelMenuSolicitada.get(i).getCantidadPedida();
        }

        return "Mesa: " + getNumeroMesa() + " Mozo:" + nombreCompletoMozo + " Nro Comensales: " +
            getNumComensales() + " Hora de incio:" + getHoraInicio().getHour() + " Hora De Cierre : " +
            getHoraCierre() + " Lista de ordenes: " + orden + " Precio total" + getTotalAPagar();

    }

}
