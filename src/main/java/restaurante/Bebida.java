package restaurante;

public class Bebida extends Menu{

    private double volumen;

    public Bebida(double precio, String categoria, int cantidadPedida, String marca, double volumen){
        super(precio, categoria, marca, cantidadPedida);
        this.volumen=volumen;
    }

    public double getVolumen() {
        return volumen;
    }

    public void setVolumen(double volumen) {
        this.volumen = volumen;
    }

    @Override
    public String toString(){
        return "Categoria: " + getCategoria() + ", Marca: " + getNombrePedido()
         + " volumen: " + getVolumen() + ", Precio: " + getPrecio();
    }
}
