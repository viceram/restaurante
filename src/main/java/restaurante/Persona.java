package restaurante;

public class Persona {
    private String nombre;
    private String apellido;

    //-------Constructor-----------------------
    public Persona(String nombre, String apellido){
        this.nombre=nombre;
        this.apellido=apellido;
    }

    //---------------setter--------------------
    public void setNombre(String nombre){
        this.nombre=nombre;
    }

    public void setApellido(String apellido){
        this.apellido=apellido;
    }

    //---------------getter--------------------
    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }
}
