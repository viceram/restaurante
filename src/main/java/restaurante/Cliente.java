package restaurante;

public class Cliente extends Persona {

    private TarjetaCredito tarjetaDelCliente;
    private Orden ordenPedida;
    private Pago formaDePago;

    // ---------------constructor---------------
    public Cliente(String nombre, String apellido, TarjetaCredito tarjetaDelCliente) {
        super(nombre, apellido);
        this.tarjetaDelCliente = tarjetaDelCliente;
    }


    public Cliente(String nombre, String apellido) {
        super(nombre, apellido);
    }

    // ---------------setter--------------------
    public void setTarjetaCliente(TarjetaCredito tarjetaDelCliente){
        this.tarjetaDelCliente=tarjetaDelCliente;
    }

    public void setOrdenPedida(Orden ordenPedida) {
        this.ordenPedida = ordenPedida;
    }

    public void setFormaDePago(Pago formaDePago) {
        this.formaDePago = formaDePago;
    }

    //---------------getter--------------------
    public TarjetaCredito getTarjetaCliente(){
        return tarjetaDelCliente;
    }

    public Orden getOrdenPedida() {
        return ordenPedida;
    }

    public Pago getFormaDePago() {
        return formaDePago;
    }

    //--------------------------------------

    public void cerrarLaOrden(){
        ordenPedida.cerrarOrden("Cerrada");
        formaDePago.setTotalPagar(ordenPedida.getTotalAPagar());
    }

    @Override
    public String toString(){
        return "Nombre: " + getNombre() + " Apellido: " + getApellido();
    }
}
