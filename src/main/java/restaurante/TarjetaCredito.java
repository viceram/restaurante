package restaurante;

import java.time.LocalDate;

public class TarjetaCredito extends Pago {
    private int numeroTarjeta;
    private Persona titular;
    private LocalDate fechaDeVencimiento;
    private double recargo;

    public TarjetaCredito(int numeroTarjeta,Persona titular, LocalDate fechaVencimiento){
        this.numeroTarjeta=numeroTarjeta;
        this.titular=titular;
        this.fechaDeVencimiento=fechaVencimiento;
    }

    //---------------setter----------------
    public void setRecargo(double recargo){
        this.recargo=recargo;
    }

    //----------------getter----------------
    public int getNumeroTarjeta(){
        return numeroTarjeta;
    }

    public Persona getTitular(){
        return titular;
    }

    public LocalDate getFechaVencimiento(){
        return fechaDeVencimiento;
    }

    public double getRecargo(){
        return recargo;
    }

    @Override
    public String efectuarPago(){
        double totalConRecargo;
        totalConRecargo=getTotalPagar() + getTotalPagar()*getRecargo();

        return "Numero de tarjeta: " + getNumeroTarjeta() + "Total: " + getTotalPagar() +
        " Recargo: " + getRecargo() + " Total con recargo: " + totalConRecargo;
    }
}
