package restaurante;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MenuTest {
    
    @Test 
    public void testDePolimorfismo(){
       Menu orden1= new Bebida(45, "Gaseosas", 2, "Fanta", 1);
       double precioPorCantidadActual;
       precioPorCantidadActual=orden1.getPrecio()*orden1.getCantidadPedida();
       double precioEsperado=90;
       String categoriaEsperada="Gaseosas";
       String categoriaActual=orden1.getCategoria();

       assertEquals(categoriaEsperada, categoriaActual);
       assertEquals(precioEsperado, precioPorCantidadActual,0.1);
    }
}
