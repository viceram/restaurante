package restaurante;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

import org.junit.Test;

public class OrdenTest {
    
    @Test
    public void testIniciarUnaOrden_CalcularTotal(){
        LocalTime horaInicio= LocalTime.now();
        Persona mozo=new Persona("Jacobo", "Diaz");

        Orden orden1=new Orden(1, mozo, horaInicio);

        String descripcionPizza = "Pizza con queso, jamon, huevo y salsa";
        Menu ordenDeBebida = new Bebida(35, "Bebidas", 2, "Sprite", 500);
        Menu ordenDeComida = new Plato(100, "Pizzas", 1, "Pizza especial", descripcionPizza);
        orden1.setOrdenSolicitada(ordenDeBebida);
        orden1.setOrdenSolicitada(ordenDeComida);
        orden1.calcularPrecioTotalDeOrden();

        String estadoActual=orden1.getEstadoOrden();
        String estadoEsperado="Abierta";

        double totalAPagarActual=orden1.getTotalAPagar();
        double totalEsperado=170;

        assertEquals(estadoEsperado, estadoActual);
        assertEquals(totalEsperado, totalAPagarActual,0.1);
    }
}
