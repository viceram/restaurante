package restaurante;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

import org.junit.Test;

public class ClienteTest {
    
    @Test
    public void testAbrirOrden_PagarYCerrar(){
        Cliente cliente1 = new Cliente("Horacio", "Soria");

        Persona mozo1 = new Persona("Martin", "Gomez");
        LocalTime horaInicio= LocalTime.now();

        Orden orden1 = new Orden(2, mozo1, horaInicio);

        String descripcionPizza = "Pizza con queso y salsa de tomate";
        Menu primerPedido = new Plato(100, "Pizza", 1, "Pizza comun", descripcionPizza);
        orden1.setOrdenSolicitada(primerPedido);

        cliente1.setOrdenPedida(orden1);

        String estadoEsperado = "Abierta";
        String estadoActual = cliente1.getOrdenPedida().getEstadoOrden();

        Efectivo efectivo = new Efectivo();
        efectivo.setEfectivoParaPago(100);
        cliente1.setFormaDePago(efectivo);

        assertEquals(estadoEsperado, estadoActual);

        cliente1.cerrarLaOrden();

        estadoEsperado="Cerrada";
        estadoActual = cliente1.getOrdenPedida().getEstadoOrden();

        assertEquals(estadoEsperado, estadoActual);
    }

}
